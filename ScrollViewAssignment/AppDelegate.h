//
//  AppDelegate.h
//  ScrollViewAssignment
//
//  Created by Jordan Hipwell on 1/12/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

