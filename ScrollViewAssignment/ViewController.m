//
//  ViewController.m
//  ScrollViewAssignment
//
//  Created by Jordan Hipwell on 1/12/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    scrollView.backgroundColor = [UIColor blackColor];
    scrollView.pagingEnabled = YES;
    
    CGFloat viewWidth = self.view.frame.size.width;
    CGFloat viewHeight = self.view.frame.size.height;
    
    //set total scrollable size of scroll view
    scrollView.contentSize = CGSizeMake(viewWidth * 3, viewHeight * 3);
    
    [self.view addSubview:scrollView];
    
    //some lovely colors
    NSArray *colorsArray = @[ [UIColor colorWithRed:252/255.0f green:61/255.0f blue:57/255.0f alpha:1.0f],
                              [UIColor colorWithRed:253/255.0f green:148/255.0f blue:9/255.0f alpha:1.0f],
                              [UIColor colorWithRed:254/255.0f green:203/255.0f blue:47/2538.0f alpha:1.0f],
                              [UIColor colorWithRed:83/255.0f green:215/255.0f blue:105/255.0f alpha:1.0f],
                              [UIColor colorWithRed:60/255.0f green:171/255.0f blue:218/255.0f alpha:1.0f],
                              [UIColor colorWithRed:21/255.0f green:126/255.0f blue:251/255.0f alpha:1.0f],
                              [UIColor colorWithRed:89/255.0f green:90/255.0f blue:211/255.0f alpha:1.0f],
                              [UIColor colorWithRed:252/255.0f green:49/255.0f blue:89/255.0f alpha:1.0f],
                              [UIColor colorWithRed:252/255.0f green:76/255.0f blue:130/255.0f alpha:1.0f] ];
    
    //add grid of fullscreen subviews - 3 x 3
    int index = 0;
    for (int y = 0; y < 3; y++) {
        for (int x = 0; x < 3; x++) {
            UIView *colorBlockView = [[UIView alloc] initWithFrame:CGRectMake(x * viewWidth, y * viewHeight, viewWidth, viewHeight)];
            colorBlockView.backgroundColor = colorsArray[index];
            
            [scrollView addSubview:colorBlockView];
            
            //add label to color block
            UILabel *label = [[UILabel alloc] initWithFrame:scrollView.frame];
            label.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5f];
            label.font = [UIFont systemFontOfSize:20];
            label.textAlignment = NSTextAlignmentCenter;
            label.text = [NSString stringWithFormat:@"Page %d", index + 1];
            
            [colorBlockView addSubview:label];
            
            index++;
        }
    }
}

@end
